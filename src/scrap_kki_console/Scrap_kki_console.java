/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scrap_kki_console;

import java.beans.Statement;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import javax.swing.JFileChooser;
import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author analyst
 */
public class Scrap_kki_console {

    public static String the_string_header = "INSERTING_STATUS#NAME#GENDER#PRACTICE_PLACE#COMPETENCE#STR_NUMBER#END_DATE_OF_STR#CITY#STATUS#URL_PHOTO#URL_DETAIL" + (System.getProperty("line.separator"));
    public static String the_string = "";
    public static String file_dir = "";
    public static String filename_without_extension = "";
    public static String filename_with_extension = "";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here

        String filepathname = "../web_source_folder/form_muhammad nurul.txt";
        String line1 = "";
        String line2 = "";

        System.out.println("filepathname" + filepathname);

        File f = new File(filepathname);
        filename_with_extension = f.getName();
        filename_without_extension = filename_with_extension.replaceFirst("[.][^.]+$", "");

        String the_timeStamp = new SimpleDateFormat("yyyy_MM_dd").format(Calendar.getInstance().getTime());

        file_dir = System.getProperty("user.home") + File.separatorChar + "My Documents";

        String file_result_name = file_dir + "\\result_folder\\" + filename_without_extension + the_timeStamp + ".txt";

        try (Stream<String> lines = Files.lines(Paths.get(filepathname))) {
            line1 = lines.findFirst().get().trim();
        } catch (Exception ex) {
            System.out.println("try catch read file" + ex);
        }

        if ("<!DOCTYPE html>".equals(line1)) {
            deleteRows(filepathname);
        }

        // <editor-fold defaultstate="collapsed" desc=" read from web_source file, get href value, scrap and display ">
        BufferedReader in_1 = null;
        try {
            in_1 = new BufferedReader(new FileReader(filepathname));
            String line = null;
            int count_href = 0;
            while ((line = in_1.readLine()) != null) {

                //performing regex to get href link
                String anchorRegex = "<\\s*a\\s+.*?href\\s*=\\s*\"(\\S*?)\".*?>";
                Pattern anchorPattern = Pattern.compile(anchorRegex);
                String content = line;
                Matcher matcher = anchorPattern.matcher(content);

                while (matcher.find()) {
                    //System.out.println(matcher.group(1));
                    String href_value = matcher.group(1);
                    scrapByUrl(href_value);
                    count_href++;
                }

                //performing regex to get href link
            }
            //System.out.println( file_result_name);
            writeToFile(the_string_header + the_string, file_result_name);
            System.out.println("total a href = " + count_href);
        } catch (IOException e) {
            System.out.println("There was a problem: " + e);
            e.printStackTrace();
        } finally {
            try {
                in_1.close();
            } catch (Exception e) {
            }
        }
        //</editor-fold>
    }

    public static void scrapByUrl(String url_param) throws IOException {
        String html = url_param;
        String nama = "";
        String jenis_kelamin = "";
        String tempat_praktek = "";
        String kompetensi = "";
        String no_str = "";
        String tgl_akhir_str = "";
        String kab_kota = "";
        String status = "";
        String srcValue = "";

        // <editor-fold defaultstate="collapsed" desc=" get data in table ">
        try {
            Document doc = Jsoup.connect(html).get();
            Elements tableElements = doc.select("table");

            Elements tableHeaderEles = tableElements.select("thead tr th");
            //System.out.println("headers");
//            for (int i = 0; i < tableHeaderEles.size(); i++) {
//                System.out.println(tableHeaderEles.get(i).text());
//            }
            //System.out.println();

            Elements tableRowElements = tableElements.select(":not(thead) tr");

            for (int i = 0; i < tableRowElements.size(); i++) {
                Element row = tableRowElements.get(i);

                if (i == 2) {
                    Elements rowItems = row.select("td");
                    for (int j = 0; j < rowItems.size(); j++) {

                        if (j == 1) {
                            nama = rowItems.get(j).text().toString().substring(1);
                            //System.out.println(nama);
                        }
                    }
                } else if (i == 3) {
                    Elements rowItems = row.select("td");
                    for (int j = 0; j < rowItems.size(); j++) {
                        if (j == 1) {
                            jenis_kelamin = rowItems.get(j).text().toString().substring(1);
                            //System.out.println(jenis_kelamin);
                        }
                    }
                } else if (i == 4) {
                    Elements rowItems = row.select("td");
                    for (int j = 0; j < rowItems.size(); j++) {

                        if (j == 1) {
                            tempat_praktek = rowItems.get(j).text().toString();
                            //System.out.println(tempat_praktek);
                        }
                    }
                } else if (i == 6) {
                    Elements rowItems = row.select("td");
                    for (int j = 0; j < rowItems.size(); j++) {
                        if (j == 1) {
                            kompetensi = rowItems.get(j).text().toString().substring(1);
                            //System.out.println(kompetensi);

                        }
                    }
                } else if (i == 7) {
                    Elements rowItems = row.select("td");
                    for (int j = 0; j < rowItems.size(); j++) {
                        if (j == 1) {
                            no_str = rowItems.get(j).text().toString().substring(1);
                            //System.out.println(no_str);   
                        }
                    }
                } else if (i == 10) {
                    Elements rowItems = row.select("td");
                    for (int j = 0; j < rowItems.size(); j++) {
                        if (j == 1) {
                            tgl_akhir_str = rowItems.get(j).text().toString().substring(1);
                            //System.out.println(tgl_akhir_str);

                        }
                    }
                } else if (i == 11) {
                    Elements rowItems = row.select("td");
                    for (int j = 0; j < rowItems.size(); j++) {
                        if (j == 1) {
                            kab_kota = rowItems.get(j).text().toString().substring(1);
                            //System.out.println(kab_kota);

                        }
                    }
                } else if (i == 13) {
                    Elements rowItems = row.select("td");
                    for (int j = 0; j < rowItems.size(); j++) {
                        if (j == 1) {
                            status = rowItems.get(j).text().toString().substring(1);
                            //System.out.println(status);
                        }

                    }
                }

            }
            // <editor-fold defaultstate="collapsed" desc=" get url photo ">
            try {
                Element imageElement = doc.select("img").first();

                //String absoluteUrl = imageElement.absUrl("src");  //absolute URL on src
                srcValue = imageElement.attr("src");  // exact content value of the attribute.

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            //</editor-fold>

            //CHECK to MYSql
            String[] the_array_string = new String[11];
            the_array_string[0] = nama;
            the_array_string[1] = jenis_kelamin;
            the_array_string[2] = tempat_praktek;
            the_array_string[3] = kompetensi;
            the_array_string[4] = no_str;
            the_array_string[5] = tgl_akhir_str;
            the_array_string[6] = nama;
            the_array_string[7] = kab_kota;
            the_array_string[8] = status;
            the_array_string[9] = url_param;
            the_array_string[10] = srcValue;

            String the_result_checkToMysql_function = checkToMysql(the_array_string);
            //CHECK to MYSql

            String the_data = the_result_checkToMysql_function.trim() + '#' + nama.trim() + '#' + jenis_kelamin.trim() + '#' + tempat_praktek.trim() + '#' + kompetensi.trim() + '#' + no_str.trim() + '#' + tgl_akhir_str.trim() + '#' + kab_kota.trim() + '#' + status.trim() + '#' + url_param.trim() + '#' + srcValue.trim();

            System.out.println(the_data);

            the_string += (the_data.trim());
            the_string += (System.getProperty("line.separator"));

        } catch (IOException e) {
            e.printStackTrace();
        }
        //</editor-fold>

    }

    public static void deleteRows(String complete_file_path) {
        try {
            int startline = 1;
            int numlines = 144;

            BufferedReader br = new BufferedReader(new FileReader(complete_file_path));

            //String buffer to store contents of the file
            StringBuffer sb = new StringBuffer("");

            //Keep track of the line number
            int linenumber = 1;
            String line;

            while ((line = br.readLine()) != null) {
                //Store each valid line in the string buffer
                if (linenumber < startline || linenumber >= startline + numlines) {
                    sb.append(line + "\n");
                }
                linenumber++;
            }
            if (startline + numlines > linenumber) {
                System.out.println("End of file reached.");
            }
            br.close();

            FileWriter fw = new FileWriter(new File(complete_file_path));
            //Write entire string buffer into the file
            fw.write(sb.toString());
            fw.close();
        } catch (Exception e) {
            System.out.println("Something went horribly wrong: " + e.getMessage());
        }
    }

    public static String checkToMysql(String[] the_string_param) {
        int numberOfRows = 0;
        String the_return = "";

        String var_str_number = "";
        String var_end_date_str_number = "";
        String var_name = "";

        String var_practice_place = "";
        String var_competence = "";
        String var_city = "";
        String var_status = "";
        String var_url_detail = "";
        String var_url_photo = "";

        var_str_number = the_string_param[4].trim();
        var_end_date_str_number = the_string_param[5].trim();
        var_name = the_string_param[6].trim();
        int var_gender = 0;
        if (the_string_param[1].matches("PRIA")) {
            var_gender = 1;
        }
        var_practice_place = the_string_param[2].trim();
        var_competence = the_string_param[3].trim();
        var_city = the_string_param[7].trim();
        var_status = the_string_param[8].trim();
        var_url_detail = the_string_param[9].trim();
        var_url_photo = the_string_param[10].trim();

        try {
            // create a mysql database connection
            String myDriver = "com.mysql.jdbc.Driver";
            String myUrl = "jdbc:mysql://localhost:3306/gsk_kki_db";
            Class.forName(myDriver);
            Connection conn = DriverManager.getConnection(myUrl, "root", "");

            String date_time_now = new SimpleDateFormat("dd-MM-yyyy_HH:mm:ss").format(new Date());

            String query_1 = "select count(*) from 12_2017 WHERE STR_NUMBER LIKE ? ";
            //String query_1 = "SELECT COUNT(*) FROM 12_2017 WHERE STR_NUMBER LIKE ? AND END_DATE_STR_NUMBER LIKE ? AND NAME LIKE ? AND GENDER LIKE ? AND PRACTICE_PLACE LIKE ? AND COMPETENCE LIKE ? AND CITY LIKE ? AND STATUS LIKE ? AND URL_DETAIL LIKE ? AND URL_PHOTO LIKE ? ";

            PreparedStatement pstmt = conn.prepareStatement(query_1);
            pstmt.setString(1,  var_str_number );
//            pstmt.setString(2, '%' + var_end_date_str_number + '%');
//            pstmt.setString(3, '%' + var_name + '%');
//            pstmt.setInt(4, var_gender);
//            pstmt.setString(5, '%' + var_practice_place + '%');
//            pstmt.setString(6, '%' + var_competence + '%');
//            pstmt.setString(7, '%' + var_city + '%');
//            pstmt.setString(8, '%' + var_status + '%');
//            pstmt.setString(9, '%' + var_url_detail.substring(50));
//            pstmt.setString(10,'%' + var_url_photo.substring(36));

            //System.out.println("After : " + pstmt.toString());
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                numberOfRows = rs.getInt(1);

                System.out.println("numberOfRows= " + numberOfRows);

                if (numberOfRows == 0) {
                    // <editor-fold defaultstate="collapsed" desc=" INSERT TO DATABASE ">
                    try {
                        // the mysql insert statement
                        String query = " insert into 12_2017 (STR_NUMBER, END_DATE_STR_NUMBER, NAME, GENDER, PRACTICE_PLACE, COMPETENCE, CITY, STATUS, URL_DETAIL, URL_PHOTO, IS_DELETED, CREATED_AT, UPDATED_AT)"
                                + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                        // create the mysql insert preparedstatement
                        PreparedStatement preparedStmt = conn.prepareStatement(query);
                        preparedStmt.setString(1, var_str_number);
                        preparedStmt.setString(2, var_end_date_str_number);
                        preparedStmt.setString(3, var_name);
                        preparedStmt.setInt(4, var_gender);
                        preparedStmt.setString(5, var_practice_place);
                        preparedStmt.setString(6, var_competence);
                        preparedStmt.setString(7, var_city);
                        preparedStmt.setString(8, var_status);
                        preparedStmt.setString(9, var_url_detail);
                        preparedStmt.setString(10, var_url_photo);
                        preparedStmt.setInt(11, 0);
                        preparedStmt.setString(12, date_time_now);
                        preparedStmt.setString(13, date_time_now);

                        // execute the preparedstatement
                        preparedStmt.execute();
                        the_return = "INSERT SUCCESS";

                        conn.close();
                    } catch (Exception ex) {
                        the_return = "INSERT UNSUCCESS";
                        System.out.println("exception insert to mysql = " + ex);
                    }
                    //</editor-fold>
                }
                
                if(numberOfRows > 0){
                    // <editor-fold defaultstate="collapsed" desc=" UPDATE ROW ">
                    try {
                        // the mysql insert statement
                        
                        String update_sql = "UPDATE 12_2017 SET END_DATE_STR_NUMBER = ?, NAME = ?, "
                                + "GENDER = ?, PRACTICE_PLACE = ?, COMPETENCE = ?, CITY = ?, STATUS = ?,"
                                + " URL_DETAIL = ?, URL_PHOTO = ?, UPDATED_AT = ? WHERE STR_NUMBER LIKE ?";

                        // create the mysql insert preparedstatement
                        PreparedStatement preparedStmt = conn.prepareStatement(update_sql);
                        
                        preparedStmt.setString(1, var_end_date_str_number);
                        preparedStmt.setString(2, var_name);
                        preparedStmt.setInt(3, var_gender);
                        preparedStmt.setString(4, var_practice_place);
                        preparedStmt.setString(5, var_competence);
                        preparedStmt.setString(6, var_city);
                        preparedStmt.setString(7, var_status);
                        preparedStmt.setString(8, var_url_detail);
                        preparedStmt.setString(9, var_url_photo);
                        preparedStmt.setString(10, date_time_now);
                        preparedStmt.setString(11, var_str_number);

                        // execute the preparedstatement
                        preparedStmt.executeUpdate();
                        the_return = "UPDATE SUCCESS";

                        conn.close();
                    } catch (Exception ex) {
                        the_return = "UPDATE UNSUCCESS";
                        System.out.println("exception UPDATE to mysql = " + ex);
                    }
                    //</editor-fold>
                }

            } else {
                System.out.println("error on select count");
            }

        } catch (Exception e) {
            System.err.println("Got an exception!");
            System.err.println(e);
        }
        return the_return;
    }

    public static void writeToFile(String the_string_param, String filename_result) {

        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            File file = new File(filename_result);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // true = append file
            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);

            bw.write(the_string_param);

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null) {
                    bw.close();
                }

                if (fw != null) {
                    fw.close();
                }

            } catch (IOException ex) {

                ex.printStackTrace();

            }
        }
    }
}
